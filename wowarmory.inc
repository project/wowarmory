<?php
/**
 * Diplay the simple roster
 * @return simple roster HTML
 */

//setup path variables to avoid function overhead with drupal_get_path 
$modulepath = drupal_get_path('module', 'wowarmory');
$imgpath = $modulepath . '/images';


//curl a given url based on the type of xml document requested
function wowarmory_curl_url($type) {
	$useragent = "User-Agent: Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.2) Gecko/20070219 Firefox/2.0.0.2";
	switch ($type) {
		case 'guild':
			if (variable_get('wowarmory_region', 'en') == 0) {
				$xml_url = "http://www.wowarmory.com/guild-info.xml?r=" . urlencode(variable_get('wowarmory_realm', '')) . "&gn=" . urlencode(variable_get('wowarmory_guild', ''));
			}
			if (variable_get('wowarmory_region', 'en') == 1) {
				$xml_url = "http://eu.wowarmory.com/guild-info.xml?r=" . urlencode(variable_get('wowarmory_realm', '')) . "&gn=" . urlencode(variable_get('wowarmory_guild', ''));
			}
			break;
		case 'character':
			break;
		case 'realm':
			break;	
	}
	//curl the xml sheet
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $xml_url);
  curl_setopt($ch, CURLOPT_HEADER, 0);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_USERAGENT, $useragent);
  //curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
	//Test to see if curl executed
	if (curl_exec($ch) === FALSE) {
		exit();
	}
  $data = curl_exec($ch);
  curl_close($ch);
	
	//Attempt at detecting maintenance mode. Working as of 3/23/2010
  if (strpos($data, "maintenance") == TRUE) {
    watchdog('wowarmory', 'Armory is in maintenance mode. No changes made.', array(), WATCHDOG_NOTICE);
    exit();
  }
  if (strpos($data, "404") == TRUE) {
    watchdog('wowarmory', 'Armory returned a 404 error. No changes made.', array(), WATCHDOG_NOTICE);
    exit();
  }
	else {
    return $data;
  }
}

function wowarmory_update_roster() {

  $doc = new DOMDocument();
  $xml = wowarmory_curl_url('guild');
  //echo $xml_str;
  $doc->loadXML($xml);
  $member_list = $doc->getElementsByTagName("character");

  /**
	 * This whole process needs to to updated to use only one query for the
	 * entire guild insertion, instead of one for each member
	 * Also fix the overall process to cut down on the total queries
  */
  $query = "SELECT name FROM {wowarmory_roster}";
  $result = db_query($query);
  $current = array();
  foreach ($result as $row) {
    $current[] = $row->name;
  }

  foreach ($member_list as $toon) {
    $data = array(
      'achPoints' => $toon->getAttribute("achPoints"),
      'classId' => $toon->getAttribute("classId"),
      'genderId'  => $toon->getAttribute("genderId"),
      'level'  => $toon->getAttribute("level"),
      'name'  => $toon->getAttribute("name"),
      'raceId'  => $toon->getAttribute("raceId"),
      'rank'  => $toon->getAttribute("rank"),
    );
    //echo '<pre>';
    //print_r($data);


    // Does the character already exist?
    // We can check this from the query that we already ran. member names are listed in the
    $query = "SELECT id FROM {wowarmory_roster} WHERE name = '" . $data['name'] . "';";

    // the character exists, update
    if ($id = db_query($query)->fetchField()) {
      $sql_update = "UPDATE {wowarmory_roster} SET level = " . $data['level'] . ", achPoints = ". $data['achPoints'] . " WHERE ID = '" . $id . "';";
      db_query($sql_update);

    }
    else {
      // the character is new, insert
      $sql_insert = "INSERT INTO {wowarmory_roster} ( achPoints, classId, genderId, level, name, raceId, rank ) " .
      "VALUES ('" . $data['achPoints'] . "', '" . $data['classId'] . "', '" . $data['genderId'] . "', '" . $data['level'] . "', '" . $data['name'] . "', '" . $data['raceId'] . "', '" . $data['rank'] . "')";
      db_query($sql_insert);
      watchdog('wowarmory', $data['name'] . ' has been added to the roster.' , array(), WATCHDOG_NOTICE);
    }

    // As we filter through each player from wowarmory.com, remove them from our tracking array.
    foreach ($current as $key => $value) {
      if ($value == $data['name']) {
        unset($current[$key]);
      }
    }

    // Update the index's for the array (This may not be required, but lets keep it clean.
    $current = array_values($current);

  }

  // Clean up the players that were not seen on the roster page.
  //echo '<br />The following players were not seen, and are being removed: ';
  foreach ($current as $name) {
    watchdog('wowarmory', $name . " has been removed from the roster.");
    db_query("DELETE FROM {wowarmory_roster} WHERE name = '%s'", $name);
  }

  watchdog('wowarmory', 'Armory updated successfully.', array(), WATCHDOG_NOTICE);
}

/**
* Retrieve the server status of the guild to display.
* @return the xhtml output for the block to display the server status.

function wowarmory_get_realmstatus() {
	return $output;
}
**/
?>